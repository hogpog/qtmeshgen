#include "ogrewidget.h"
#include "SpaceCamera.h"
#include "InputManager.h"
#include <QDebug>
#include <QTimer>
#include "ImageDetection.h"
#include "Delaunay.h"

void OgreWidget::keyPressEvent(QKeyEvent *event)
{
    if (mOgreWidgetFocus)
    {
        switch (event->key()) {
             case Qt::Key_Left:
                //tryMove(curPiece, curX - 1, curY);
                qDebug()<<"left";
                 //break;
             case Qt::Key_Right:
                qDebug()<<"right";
                 //tryMove(curPiece, curX + 1, curY);
                 //break;
             case Qt::Key_Down:
                qDebug()<<"down";
                 //tryMove(curPiece.rotatedRight(), curX, curY);
                 //break;
             case Qt::Key_Up:
                qDebug()<<"up";
                 //tryMove(curPiece.rotatedLeft(), curX, curY);
                 break;
        }
    }
    event->ignore();
}
void OgreWidget::keyReleaseEvent(QKeyEvent *event)
{
    if (mOgreWidgetFocus)
    {
       // qDebug()<<"tecla solta com foco";
    }
    event->ignore();

}
void OgreWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (mOgreWidgetFocus)
    {
        //qDebug()<<"mouse move com foco";
    }
    event->ignore();


}
void OgreWidget::mousePressEvent(QMouseEvent *event)
{
    mOgreWidgetFocus = this->rect().contains(this->mapFromGlobal(QCursor::pos()));
    if(mOgreWidgetFocus)
    {
       // qDebug()<<"mouse press com foco";
    }
    event->ignore();

}
void OgreWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(!mOgreWidgetFocus && !this->rect().contains(this->mapFromGlobal(QCursor::pos())))
    {
        mOgreWidgetFocus = false;
    }
    if (mOgreWidgetFocus)
    {
       // qDebug()<<"mouse release com foco";
    }
    event->ignore();

}

void OgreWidget::init( std::string plugins_file,
         std::string ogre_cfg_file,
         std::string ogre_log )
{
  // create the main ogre object
  mOgreRoot = Manager::getRoot();

  // setup a renderer
    Ogre::RenderSystemList::const_iterator renderers = mOgreRoot->getAvailableRenderers().begin();
    while(renderers != mOgreRoot->getAvailableRenderers().end())
    {
        Ogre::String rName = (*renderers)->getName();
          if (rName == "OpenGL Rendering Subsystem")
              break;
          renderers++;
    }

    Ogre::RenderSystem *renderSystem = *renderers;

  mOgreRoot->setRenderSystem( renderSystem );
  QString dimensions = QString( "%1x%2" ).arg(this->width()).arg(this->height());

  renderSystem->setConfigOption( "Video Mode", dimensions.toStdString() );

  // initialize without creating window
  //mOgreRoot->getRenderSystem()->setConfigOption( "Full Screen", "No" );
  //mOgreRoot->saveConfig();
  mOgreRoot->initialise(false); // don't create a window

}

/**
 * @brief setup the rendering context
 * @author Kito Berg-Taylor
 */
void OgreWidget::initializeGL()
{
    makeCurrent();
  //== Creating and Acquiring Ogre Window ==//

  // Get the parameters of the window QT created
  Ogre::String winHandle;
  #ifdef WIN32
  // Windows code
  winHandle += Ogre::StringConverter::toString((unsigned long)(this->parentWidget()->winId()));
  #else
  // Unix code
  QX11Info info = x11Info();
  winHandle  = Ogre::StringConverter::toString((unsigned long)(info.display()));
  winHandle += ":";
  winHandle += Ogre::StringConverter::toString((unsigned int)(info.screen()));
  winHandle += ":";
  winHandle += Ogre::StringConverter::toString((unsigned long)(this->parentWidget()->winId()));
  #endif

  Ogre::NameValuePairList params;
  params["parentWindowHandle"] = winHandle;
  //params["currentGLContext"] = "true";
  //params["externalWindowHandle"] = Ogre::StringConverter::toString((unsigned long)(this->parentWidget()->winId()));

  mOgreWindow = mOgreRoot->createRenderWindow( "QOgreWidget_RenderWindow",
                           this->width(),
                           this->height(),
                           false,
                           &params );
  WId ogreWinId = 0x0;
  mOgreWindow->getCustomAttribute( "WINDOW", &ogreWinId );

  assert( ogreWinId );

  //this->create( ogreWinId );
  /*setAttribute( Qt::WA_PaintOnScreen, true );
  setAttribute( Qt::WA_NoBackground );*/
  setAttribute(Qt::WA_OpaquePaintEvent);
      setAttribute(Qt::WA_PaintOnScreen);
      setAttribute(Qt::WA_NoBackground);

  //== Ogre Initialization ==//
  Ogre::SceneType scene_manager_type = Ogre::ST_GENERIC;

  mSceneMgr = mOgreRoot->createSceneManager( scene_manager_type );
  Manager::setSceneMgr(mSceneMgr);
  mSceneMgr->setAmbientLight( Ogre::ColourValue(1,1,1) );

  mCamera = mSceneMgr->createCamera( "QOgreWidget_Cam" );
  mCamera->setPosition( Ogre::Vector3(0,100,0) );
  mCamera->lookAt( Ogre::Vector3(0,0,0) );
  mCamera->setNearClipDistance( 1.0 );

  Ogre::Viewport *mViewport = mOgreWindow->addViewport( mCamera );
  mViewport->setBackgroundColour( Ogre::ColourValue( 0,0,0 ) );
  SpaceCamera::Initialize(mCamera);
  SpaceCamera::GetInstance()->SetTarget(mSceneMgr->getRootSceneNode()->createChildSceneNode());
  //QtInputManager::getIntance().AddKeyListener(this);
  //QtInputManager::getIntance().AddMouseListener(this);



    Ogre::ConfigFile cf;
    cf.load("Resources.cfg");
    //Ogre::String Res;
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        Ogre::String secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
        for (Ogre::ConfigFile::SettingsMultiMap::iterator i = settings->begin();
              i != settings->end(); ++i)
        {
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(i->second, i->first, secName);
        }
    }

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    //Manager::getSceneMgr()->setSkyDome(true, "Sky/CloudySky", 5, 8,20);
    //Manager::getSceneMgr()->setFog(Ogre::FOG_LINEAR,Ogre::ColourValue::White,0.001,400,450);

    vertexSet mVertexSet;
    triangleSet mTriangleSet;

    ImageDetection img;
    std::deque<pontos> lista = img.getPoints();

    QPoint vetor;
    int angle = 0;
    QLineF line;
    for(int x=0;x<lista.size();x++)
    {
        line.setPoints(lista[x].a,lista[x].b);
        angle += line.angle();
        vetor = QPoint(vetor+QPoint(lista[x].a.x()-lista[x].b.x(),lista[x].a.y()-lista[x].b.y()));
        float diff = (lista[x].a.x()-lista[x].b.x());
        float z = diff*diff;
        diff = (lista[x].a.y()-lista[x].b.y());
        z += diff*diff;
        z = sqrt(z);
       /* if(lista[x].a.x()>lista[x].b.x()&&lista[x].a.y()==lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(0,0,128)));
        else if(lista[x].a.x()>lista[x].b.x()&&lista[x].a.y()>lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(0,128,0)));
        else if(lista[x].a.x()==lista[x].b.x()&&lista[x].a.y()>lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(128,0,0)));
        else if(lista[x].a.x()<lista[x].b.x()&&lista[x].a.y()>lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(128,128,0)));
        else if(lista[x].a.x()<lista[x].b.x()&&lista[x].a.y()==lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(128,0,128)));
        else if(lista[x].a.x()<lista[x].b.x()&&lista[x].a.y()<lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(0,128,128)));
        else if(lista[x].a.x()==lista[x].b.x()&&lista[x].a.y()<lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(50,50,50)));
        else if(lista[x].a.x()>lista[x].b.x()&&lista[x].a.y()<lista[x].b.y())
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(128,128,128)));
        else
            mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,QColor(255,255,255)));*/
        mVertexSet.insert(vertex(lista[x].a.x()*10, lista[x].a.y()*10,z*-50,lista[x].c));
    }
    qDebug()<<"vetor"<<vetor/lista.size()<<"angle"<<angle/lista.size();
    Delaunay * mDelaunay = new Delaunay();
    mDelaunay->Triangulate(mVertexSet,mTriangleSet);

    triangleSet::iterator it = mTriangleSet.begin();
    triangleSet::iterator itEnd = mTriangleSet.end();
    Ogre::ManualObject* manual2 =  Manager::getSceneMgr()->createManualObject("manual2");
    manual2->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_STRIP);
    while (it != itEnd)
    {
        triangle t = *it;
        manual2->position(t.GetVertex(0)->GetX()*-1,t.GetVertex(0)->GetY()*-1,t.GetVertex(0)->GetZ());
        manual2->colour(t.GetVertex(0)->GetC().redF(),t.GetVertex(0)->GetC().greenF(),t.GetVertex(0)->GetC().blueF());
        manual2->position(t.GetVertex(1)->GetX()*-1,t.GetVertex(1)->GetY()*-1,t.GetVertex(1)->GetZ());
        manual2->colour(t.GetVertex(1)->GetC().redF(),t.GetVertex(1)->GetC().greenF(),t.GetVertex(1)->GetC().blueF());
        manual2->position(t.GetVertex(2)->GetX()*-1,t.GetVertex(2)->GetY()*-1,t.GetVertex(2)->GetZ());
        manual2->colour(t.GetVertex(2)->GetC().redF(),t.GetVertex(2)->GetC().greenF(),t.GetVertex(2)->GetC().blueF());
        ++it;
    }
    manual2->end();
    Manager::getSceneMgr()->getRootSceneNode()->attachObject(manual2);
    Ogre::MeshSerializer ms;
    ms.exportMesh(manual2->convertToMesh("meshconvertida").getPointer(),"mesh.mesh");

    timer = new QTimer;

    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(30);



}
void OgreWidget::FindCommonPoints(std::pair<EdgeAlgebra::Site *, EdgeAlgebra::Site *> &line)
{
    std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> firstLine;
    std::vector<std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> > commonLines;

    foreach (firstLine, mLineList)
    {
        if(line.first == firstLine.first ||line.second == firstLine.first || line.first == firstLine.second || line.second == firstLine.second)
        {
            commonLines.push_back(firstLine);
        }
    }
    CreatePoly(commonLines, line);


}

void OgreWidget::CreatePoly(std::vector<std::pair<EdgeAlgebra::Site *, EdgeAlgebra::Site *> > &linelist,std::pair<EdgeAlgebra::Site *, EdgeAlgebra::Site *> &baseline)
{


    std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> line;
    std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> line2;
    foreach(line,linelist)
    {
        foreach(line2,linelist)
        {
            Triangle* t = new Triangle;
            t->SetPoint1(baseline.first);
            t->SetPoint2(baseline.second);
            //as linhas precisam ter pontos iguais mas nao podem pertencer � linha principal
            if((line.first==line2.first || line.first==line2.second)&& (line.first!=baseline.first||line.first!=baseline.second))
            {
                //se nao for ele mesmo
                if(!(line.first==line2.first&&line.second==line2.second))
                {
                    t->SetPoint3(line.first);
                    bool contains = false;
                    foreach(Triangle* t1, mTriangles)
                    {
                        if(t1->Equals(t))
                        {
                            contains=true;
                            break;
                        }
                    }
                    if(!contains)
                        mTriangles.push_back(t);
                    else
                        delete t;
                }
            }
            else
            if((line.second==line2.first || line.second==line2.second)&& (line.second!=baseline.first||line.second!=baseline.second))
            {
                //se nao for ele mesmo
                if(!(line.first==line2.first&&line.second==line2.second))
                {
                    t->SetPoint3(line.second);
                    bool contains = false;
                    foreach(Triangle* t1, mTriangles)
                    {
                        if(t1->Equals(t))
                        {
                            contains=true;
                            break;
                        }
                    }
                    if(!contains)
                        mTriangles.push_back(t);
                    else
                        delete t;
                }
            }
            else
            {
                delete t;
            }
        }
    }
}

/**
 * @brief render a frame
 * @author Kito Berg-Taylor
 */
void OgreWidget::paintGL()
{
  assert( mOgreWindow );
  //Manager::getInputManager()->Capture();
  mOgreRoot->renderOneFrame();
  //QTimer::singleShot(3, this, SLOT(update()));
  //timer->start(30);
  //swapBuffers();
}

/**
 * @brief resize the GL window
 * @author Kito Berg-Taylor
 */
void OgreWidget::resizeGL( int width, int height )
{
  assert( mOgreWindow );
   mOgreWindow->resize(( float )width, ( float )height);
  mOgreWindow->windowMovedOrResized();
  mCamera->setAspectRatio( ( float ) width / ( float ) height );
  mOgreWindow->getViewport(0)->_updateDimensions();

  //QGLWidget::resizeGL(width, height);
  //mOgreWindow->windowMovedOrResized();
}

/**
 * @brief choose the right renderer
 * @author Kito Berg-Taylor
 */
Ogre::RenderSystem* OgreWidget::chooseRenderer( Ogre::RenderSystemList *renderers )
{
  // It would probably be wise to do something more friendly
  // that just use the first available renderer
  return *renderers->begin();
}
