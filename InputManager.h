
/*
 *
 *
 *
 *
 *
 */

#ifndef _InputManager_h
#define _InputManager_h

#include <map>
#include <deque>
#include <OISMouse.h>
#include <OISKeyboard.h>
#include <OISJoyStick.h>
#include <OISInputManager.h>

#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>


class InputManager : public OIS::KeyListener, public OIS::MouseListener, public OIS::JoyStickListener
{
    public:
    virtual ~InputManager();
    static InputManager* getSingletonPtr();

    void Init(Ogre::RenderWindow *renderWindow);
    void Quit();
    void Capture();

    void addKeyListener(OIS::KeyListener *keyListener, const std::string& name);
    void addMouseListener(OIS::MouseListener *mouseListener, const std::string& name);
    void addJoystickListener(OIS::JoyStickListener *joystickListener, const std::string& name);

    void removeKeyListener(const std::string& name);
    void removeMouseListener(const std::string& name);
    void removeJoystickListener(const std::string& name);

    void removeKeyListener(OIS::KeyListener *keyListener);
    void removeMouseListener(OIS::MouseListener *mouseListener);
    void removeJoystickListener(OIS::JoyStickListener *joystickListener);

    void removeAllListeners();
    void removeAllKeyListeners();
    void removeAllMouseListeners();
    void removeAllJoystickListeners();

    void setWindowExtents(int width, int height);

    OIS::Mouse*    getMouse();
    OIS::Keyboard* getKeyboard();
    OIS::JoyStick* getJoystick(unsigned int index);

    int getNumOfJoysticks();

private:
    InputManager();
    InputManager(const InputManager&) { }
    InputManager& operator=(const InputManager&);

    bool keyPressed(const OIS::KeyEvent &e);
    bool keyReleased(const OIS::KeyEvent &e);

    bool mouseMoved(const OIS::MouseEvent &e);
    bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool povMoved(const OIS::JoyStickEvent &e, int pov);
    bool axisMoved(const OIS::JoyStickEvent &e, int axis);
    bool sliderMoved(const OIS::JoyStickEvent &e, int sliderID);
    bool buttonPressed(const OIS::JoyStickEvent &e, int button);
    bool buttonReleased(const OIS::JoyStickEvent &e, int button);

    void CleanListeners();

    std::deque<std::string> mRemoveKeyListeners;
    std::deque<std::string> mRemoveMouseListeners;
    std::deque<std::string> mRemoveJoystickListeners;

    std::deque<OIS::KeyListener*> mRemoveKeyListeners2;
    std::deque<OIS::MouseListener*> mRemoveMouseListeners2;
    std::deque<OIS::JoyStickListener*> mRemoveJoystickListeners2;

    bool mQuit;

    OIS::InputManager* mInputSystem;
    OIS::Mouse*        mMouse;
    OIS::Keyboard*     mKeyboard;

    std::vector<OIS::JoyStick*> mJoysticks;
    std::vector<OIS::JoyStick*>::iterator itJoystick;
    std::vector<OIS::JoyStick*>::iterator itJoystickEnd;

    std::map<std::string, OIS::KeyListener*> mKeyListeners;
    std::map<std::string, OIS::MouseListener*> mMouseListeners;
    std::map<std::string, OIS::JoyStickListener*> mJoystickListeners;

    std::map<std::string, OIS::KeyListener*>::iterator itKeyListener;
    std::map<std::string, OIS::MouseListener*>::iterator itMouseListener;
    std::map<std::string, OIS::JoyStickListener*>::iterator itJoystickListener;

    std::map<std::string, OIS::KeyListener*>::iterator itKeyListenerEnd;
    std::map<std::string, OIS::MouseListener*>::iterator itMouseListenerEnd;
    std::map<std::string, OIS::JoyStickListener*>::iterator itJoystickListenerEnd;

    static InputManager *mInputManager;
};

#endif
