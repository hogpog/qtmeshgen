/********************************************************************************
** Form generated from reading UI file 'QEditor.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QEDITOR_H
#define UI_QEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QEditor
{
public:
    QAction *actionSave;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QDockWidget *dockWidget_2;
    QWidget *dockWidgetContents_2;

    void setupUi(QMainWindow *QEditor)
    {
        if (QEditor->objectName().isEmpty())
            QEditor->setObjectName(QStringLiteral("QEditor"));
        QEditor->resize(788, 565);
        actionSave = new QAction(QEditor);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        centralWidget = new QWidget(QEditor);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QEditor->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QEditor);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 788, 21));
        QEditor->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QEditor);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QEditor->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(QEditor);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QEditor->setStatusBar(statusBar);
        dockWidget = new QDockWidget(QEditor);
        dockWidget->setObjectName(QStringLiteral("dockWidget"));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        dockWidget->setWidget(dockWidgetContents);
        QEditor->addDockWidget(static_cast<Qt::DockWidgetArea>(1), dockWidget);
        dockWidget_2 = new QDockWidget(QEditor);
        dockWidget_2->setObjectName(QStringLiteral("dockWidget_2"));
        dockWidgetContents_2 = new QWidget();
        dockWidgetContents_2->setObjectName(QStringLiteral("dockWidgetContents_2"));
        dockWidget_2->setWidget(dockWidgetContents_2);
        QEditor->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidget_2);

        mainToolBar->addAction(actionSave);

        retranslateUi(QEditor);

        QMetaObject::connectSlotsByName(QEditor);
    } // setupUi

    void retranslateUi(QMainWindow *QEditor)
    {
        QEditor->setWindowTitle(QApplication::translate("QEditor", "QEditor", 0));
        actionSave->setText(QApplication::translate("QEditor", "Save", 0));
    } // retranslateUi

};

namespace Ui {
    class QEditor: public Ui_QEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QEDITOR_H
