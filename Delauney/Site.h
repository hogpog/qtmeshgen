/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file Site.h
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the DelaunayTriangulation class implementation.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#ifndef _Site_H_
#define _Site_H_

#include <QColor>

namespace EdgeAlgebra
{

class QuadEdge;

class Site
{
public:
  float m_x;

  float m_y;

  float m_z;

  QColor cor;

  Site(void);

  Site(float x, float y);

  Site(float x, float y, float z);

  Site(float x, float y, float z, QColor cor);

  Site(const Site& site);

  ~Site(void);

  Site& operator=(const Site& site);

  bool CounterClockWise(Site* b, Site* c);

  bool InCircle(Site* a, Site* b, Site* c);

  bool RightOf(const QuadEdge& edge);

  bool LeftOf(const QuadEdge& edge);
};

bool SiteLessThan(EdgeAlgebra::Site* left, EdgeAlgebra::Site* right);

}

#endif
