#include "ImageDetection.h"
#include <QImage>
#include <QDebug>
#include <QTime>
#include <QThread>

#include "ThreadExec.hpp"

inline float SquareRootFloat(float number) {
    long i;
    float x, y;
    const float f = 1.5F;

    x = number * 0.5F;
    y  = number;
    i  = * ( long * ) &y;
    i  = 0x5f3759df - ( i >> 1 );
    y  = * ( float * ) &i;
    y  = y * ( f - ( x * y * y ) );
    y  = y * ( f - ( x * y * y ) );
    return number * y;
}

inline int abs(int x) {
    asm
    (
     "cdq   ;"
     "xor   %%edx,%%eax ;"
     "sub   %%edx,%%eax ;"
     :"=a"(x)
     :"a"(x)
    );
   return x;
}

void ImageDetection::Detect(const QPoint &lista)
{

    unsigned long lmenor = 2000;//quantidade de erro aceit�vel
    unsigned long menor = 0;
    QPoint menorp=lista, ponto1,ponto2;
    bool achou = false;
    unsigned distanciamaior = 10;
    unsigned distanciamaiory = 2;
    unsigned x = lista.x()+distanciamaior>img.size().width()-quadrado-1?img.size().width()-quadrado-1:lista.x()+distanciamaior;

      for(x; x>quadrado&&x>lista.x()-distanciamaior ;--x)
      {

          for(unsigned y = lista.y()+distanciamaiory>img.size().height()-quadrado-1?img.size().height()-quadrado-1:lista.y()+distanciamaiory
              ; y>quadrado&&y>lista.y()-distanciamaiory ;--y)
          {

          ponto1 = QPoint(lista.x(),lista.y());
          ponto2 = QPoint(x,y);
        if(img.valid(ponto1)&&img.valid(ponto2))
        {

            for (int i = quadrado; i>=quadrado*-1&&menor<lmenor;--i)
              {
                  for (int j = quadrado; j>=quadrado*-1&&menor<lmenor;--j)
                  {
                      ponto1 = QPoint(lista.x()+i,lista.y()+j);
                      ponto2 = QPoint(x+i,y+j);
                      int r = QColor(img.pixel(ponto1)).red()- QColor(img2.pixel(ponto2)).red();

                      if(r*r>50)
                      menor += 1;
                }
              }

          if(menor<lmenor)
          {
              menorp = QPoint(x,y);
              lmenor=menor;
              achou = true;
          }
          if(menor==0)
          {
              x=quadrado+1;
              y=quadrado+1;
          }
          menor = 0;
        }
      }
      }
    if(achou)
    {
      mMutex.lock();
        lista3.push_back(pontos(lista,menorp,img3.pixel(lista)));
      mMutex.unlock();
    }
}

ImageDetection::ImageDetection()
{
quadrado=15;

        img = QImage("tsukuba_l.jpg");
        if(img.isNull())
        {
            qDebug()<<"SEM IMG!";exit(0);
        }
        img2 = QImage("tsukuba_r.jpg");
        img3 = img;

        std::deque<QPoint> lista;

///escala de cinza
        for(unsigned x = 0; x<img.size().width();x++)
        {
            for(unsigned y = 0; y<img.size().height();y++)
            {
                int cor=0;
                if(img.valid(x+1,y))
                {
                    cor = QColor(img.pixel(x,y)).red();
                    cor += QColor(img.pixel(x,y)).green();
                    cor += QColor(img.pixel(x,y)).blue();

                    cor /= 3;


                    img.setPixel(x,y,qRgb(cor,cor,cor));
                }
            }
        }

        for(unsigned x = 0; x<img2.size().width();x++)
        {
            for(unsigned y = 0; y<img2.size().height();y++)
            {
                int cor=0;
                if(img2.valid(x+1,y))
                {
                    cor = QColor(img2.pixel(x,y)).red();
                    cor += QColor(img2.pixel(x,y)).green();
                    cor += QColor(img2.pixel(x,y)).blue();

                    cor /= 3;

                    img2.setPixel(x,y,qRgb(cor,cor,cor));
                }
            }
        }
/*
        ///aplica mascara |+|-|
        for(unsigned x = 0; x<img.size().width();x++)
        {
            for(unsigned y = 0; y<img.size().height();y++)
            {
                int cor=0;
                if(img.valid(x+1,y))
                {
                    cor = QColor(img.pixel(x,y)).red()-QColor(img.pixel(x+1,y)).red()>0?QColor(img.pixel(x,y)).red()-QColor(img.pixel(x+1,y)).red():QColor(img.pixel(x+1,y)).red()-QColor(img.pixel(x,y)).red();

                    img.setPixel(x,y,qRgb(cor,cor,cor));
                }
            }
        }

        for(unsigned x = 0; x<img2.size().width();x++)
        {
            for(unsigned y = 0; y<img2.size().height();y++)
            {
                int cor=0;
                if(img2.valid(x+1,y))
                {
                    cor = QColor(img2.pixel(x,y)).red()-QColor(img2.pixel(x+1,y)).red()>0?QColor(img2.pixel(x,y)).red()-QColor(img2.pixel(x+1,y)).red():QColor(img2.pixel(x+1,y)).red()-QColor(img2.pixel(x,y)).red();

                    img2.setPixel(x,y,qRgb(cor,cor,cor));
                }
            }
        }*/

               /// define a borda da imagem 1(Verifica se o ponto tem alguma diferen�a significativa entre os pontos em volta)
                       qDebug()<<"borda inicio";
                       int dif_cor_borda = 0;
                for(int x = quadrado; x<img.size().width()-quadrado;x++)
                {
                    for(int y = quadrado; y<img.size().height()-quadrado;y++)
                    {
                        int r=0,g=0,b=0;
                        int r2=0,g2=0,b2=0;
                        int r3=0,g3=0,b3=0;
                        int r4=0,g4=0,b4=0;
                        if(img.valid(x+1,y))
                        {
                            /// Verifica na horizontal
                            r = QColor(img.pixel(x,y)).red()-QColor(img.pixel(x+1,y)).red()>0?QColor(img.pixel(x,y)).red()-QColor(img.pixel(x+1,y)).red():QColor(img.pixel(x+1,y)).red()-QColor(img.pixel(x,y)).red();
                            g = QColor(img.pixel(x,y)).green()-QColor(img.pixel(x+1,y)).green()>0?QColor(img.pixel(x,y)).green()-QColor(img.pixel(x+1,y)).green():QColor(img.pixel(x+1,y)).green()-QColor(img.pixel(x,y)).green();
                            b = QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x+1,y)).blue()>0?QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x+1,y)).blue():QColor(img.pixel(x+1,y)).blue()-QColor(img.pixel(x,y)).blue();
                        }
                        if(img.valid(x,y+1))
                        {
                            /// Verifica na vertical
                            r2 = QColor(img.pixel(x,y)).red()-QColor(img.pixel(x,y+1)).red()>0?QColor(img.pixel(x,y)).red()-QColor(img.pixel(x,y+1)).red():QColor(img.pixel(x,y+1)).red()-QColor(img.pixel(x,y)).red();
                            g2 = QColor(img.pixel(x,y)).green()-QColor(img.pixel(x,y+1)).green()>0?QColor(img.pixel(x,y)).green()-QColor(img.pixel(x,y+1)).green():QColor(img.pixel(x,y+1)).green()-QColor(img.pixel(x,y)).green();
                            b2 = QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x,y+1)).blue()>0?QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x,y+1)).blue():QColor(img.pixel(x,y+1)).blue()-QColor(img.pixel(x,y)).blue();
                        }
                        if(img.valid(x-1,y))
                        {
                            /// Verifica na vertical
                            r3 = QColor(img.pixel(x,y)).red()-QColor(img.pixel(x-1,y)).red()>0?QColor(img.pixel(x,y)).red()-QColor(img.pixel(x-1,y)).red():QColor(img.pixel(x-1,y)).red()-QColor(img.pixel(x,y)).red();
                            g3 = QColor(img.pixel(x,y)).green()-QColor(img.pixel(x-1,y)).green()>0?QColor(img.pixel(x,y)).green()-QColor(img.pixel(x-1,y)).green():QColor(img.pixel(x-1,y)).green()-QColor(img.pixel(x,y)).green();
                            b3 = QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x-1,y)).blue()>0?QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x-1,y)).blue():QColor(img.pixel(x-1,y)).blue()-QColor(img.pixel(x,y)).blue();
                        }
                        if(img.valid(x,y-1))
                        {
                            /// Verifica na vertical
                            r4 = QColor(img.pixel(x,y)).red()-QColor(img.pixel(x,y-1)).red()>0?QColor(img.pixel(x,y)).red()-QColor(img.pixel(x,y-1)).red():QColor(img.pixel(x,y-1)).red()-QColor(img.pixel(x,y)).red();
                            g4 = QColor(img.pixel(x,y)).green()-QColor(img.pixel(x,y-1)).green()>0?QColor(img.pixel(x,y)).green()-QColor(img.pixel(x,y-1)).green():QColor(img.pixel(x,y-1)).green()-QColor(img.pixel(x,y)).green();
                            b4 = QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x,y-1)).blue()>0?QColor(img.pixel(x,y)).blue()-QColor(img.pixel(x,y-1)).blue():QColor(img.pixel(x,y-1)).blue()-QColor(img.pixel(x,y)).blue();
                        }
                        if(r>=dif_cor_borda||g>=dif_cor_borda||b>=dif_cor_borda
                                ||r2>=dif_cor_borda||g2>=dif_cor_borda||b2>=dif_cor_borda
                                ||r3>=dif_cor_borda||g3>=dif_cor_borda||b3>=dif_cor_borda
                                ||r4>=dif_cor_borda||g4>=dif_cor_borda||b4>=dif_cor_borda)
                        {
                            if(img.valid(x,y))
                            lista.push_back(QPoint(x,y));
                        }
                    }
                }

         qDebug()<<"borda fim";
         QTime t;
         t.start();
         ThreadExec<boost::function<void(QPoint)> > t_e(boost::bind(&ImageDetection::Detect,this,_1));
         t_e.Execute<std::deque<QPoint> >(lista);
         qDebug()<<"ThreadExec fim";
/*

int quadrado = 2;
         for (unsigned c=0; c<lista.size(); c++)
              {

                  unsigned long lmenor = 999999999;
                  unsigned long menor = 0;
                  QPoint menorp, ponto1,ponto2;
                  if(lista[c].x()>3)
                  {
                      for(unsigned x = lista[c].x(); x>3 ;x--)
                      {
                          menor = 0;

                          ponto1 = QPoint(lista[c].x(),lista[c].y());
                          ponto2 = QPoint(x,lista[c].y());
                        if(img.valid(ponto1)&&img.valid(ponto2))
                        {


                              for (int i = quadrado*-1; i<=quadrado&&menor<lmenor;i++)
                              {
                                  for (int j = quadrado*-1; j<=quadrado&&menor<lmenor;j++)
                                  {
                                      ponto1 = QPoint(lista[c].x()+i,lista[c].y()+j);
                                      ponto2 = QPoint(x+i,lista[c].y()+j);
                                      if(img.valid(ponto1)&&img.valid(ponto2))
                                    {
                                      menor += powl(QColor(img.pixel(ponto1)).red()- QColor(img2.pixel(ponto2)).red(),2)
                                              +powl(QColor(img.pixel(ponto1)).green()-QColor(img2.pixel(ponto2)).green(),2)
                                              +powl(QColor(img.pixel(ponto1)).blue()-QColor(img2.pixel(ponto2)).blue(),2);

                                     }
                                }
                              }

                          if(menor<lmenor)
                          {
                              menorp = QPoint(x,lista[c].y());
                              lmenor=menor;
                          }
                        }
                      }
                  }
                  if(SquareRootFloat(pow(lista[c].x()-menorp.x(),2))<15.0f)
                  {
                    lista3.push_back(pontos(lista[c],menorp,img.pixel(lista[c])));
                  }
              }*/
         //ThreadExec<boost::function<void(X)> > t_e(boost::bind(&foo1::print,f,_1));
         //t_e.Execute<std::deque<X> >(v);

         //BuscaThread b;
         //b.run(lista3,lista,img,img2,2,2,0);
         //b.run(lista3,lista,img,img2,2,2,1);

               qDebug()<<"lista"<<lista3.size();
               qDebug()<<"tempo"<<t.elapsed();


}
