#include "SpaceCamera.h"
#include "InputManager.h"
#include <QDebug>

#define CAMERA_VEL 80

SpaceCamera* SpaceCamera::mSpaceCamera = NULL;
SpaceCamera * SpaceCamera::GetInstance()
{
    if(mSpaceCamera==NULL)
        throw std::logic_error("TPCamera not initialized!");
    return mSpaceCamera;
}

SpaceCamera * SpaceCamera::Initialize(Ogre::Camera* camera)
{
    if(mSpaceCamera==NULL)
        mSpaceCamera = new SpaceCamera(camera);
    return mSpaceCamera;
}
SpaceCamera::SpaceCamera(Ogre::Camera *camera)
    :mAuxPosition(Ogre::Vector3::ZERO)
    ,mTempAuxPosition(Ogre::Vector3::ZERO)
    ,mSceneMgr(Manager::getSceneMgr())
    ,mCameraNode(NULL)
    ,mCamera(NULL)
    ,fCameraDistance(10)
    ,fTempCameraDistance(10)
    ,mRightMousePressed(false)
    ,mRotX(0)
    ,mRotY(0)
    ,mKeyW(false)
    ,mKeyS(false)
    ,mKeyA(false)
    ,mKeyD(false)
    ,mKeySpace(false)
    ,mKeyX(false)
    ,mKeyShift(false)
    ,BoostVel(1)
{
    mCameraNode = mSceneMgr->getRootSceneNode()->createChildSceneNode ("TPCameraChildSceneNode");
    mCamera = camera;
    mCamera->setFarClipDistance(999999999);
    if (mCamera == 0)
        throw std::logic_error("TPCamera::TPCamera - 'Ogre::Camera* camera' == NULL");

    Manager::getRoot()->addFrameListener(this);
    /*InputManager::getSingletonPtr()->addMouseListener(this,"SpaceCameraMouseListener");
    InputManager::getSingletonPtr()->addKeyListener(this,"SpaceCameraKeyListener");*/
    QtInputManager::getIntance().AddKeyListener(this);
    QtInputManager::getIntance().AddMouseListener(this);
    //GUI::AddLayout("PositionLayer");
}

SpaceCamera::~SpaceCamera()
{
    //dtor
}

void SpaceCamera::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button()==Qt::RightButton)
    {
        mRightMousePressed = false;
    }
    qDebug()<<"mouse na camera released";
    //return true;
}

void SpaceCamera::mousePressEvent(QMouseEvent *event)
{
    if (event->button()==Qt::RightButton)
    {
        mRightMousePressed = true;
    }
    mLastX = event->x();
    mLastY = event->y();
    qDebug()<<"mouse na camera pressed";
    //return true;
}

void SpaceCamera::SetTarget(Ogre::SceneNode* target)
{
    mAuxPosition = Ogre::Vector3::ZERO;
    mTarget = target;
}


void SpaceCamera::mouseMoveEvent(QMouseEvent *event)
{
    if (!mRightMousePressed)
    {
        //fCameraDistance -= event->x()/15;
    }
    else
    {

        const float rotateSpeed = 0.8;
        mRotX += (event->y()-mLastY)* -rotateSpeed;
        mRotY += (event->x()-mLastX) * -rotateSpeed;

        mLastX = event->x();
        mLastY = event->y();

        mHeading = Ogre::Degree(mRotY);

        if (mRotX<=-89)
        {
            mRotX=-89;
        }
        if (mRotX>=89)
        {
            mRotX=89;
        }
    }
    //return true;
}

bool SpaceCamera::frameEnded(const Ogre::FrameEvent& event)
{
    return true;
}

bool SpaceCamera::frameStarted(const Ogre::FrameEvent& event)
{

    if (mKeyW)
    {
        mAuxPosition+=Ogre::Vector3(cos(mCameraHeading.valueRadians()+3.14/2)*event.timeSinceLastFrame*CAMERA_VEL,0,0)*BoostVel;
        mAuxPosition+=Ogre::Vector3(0,0,-sin(mCameraHeading.valueRadians()+3.14/2)*event.timeSinceLastFrame*CAMERA_VEL)*BoostVel;
    }
    if (mKeyS)
    {
        mAuxPosition-=Ogre::Vector3(cos(mCameraHeading.valueRadians()+3.14/2)*event.timeSinceLastFrame*CAMERA_VEL,0,0)*BoostVel;
        mAuxPosition-=Ogre::Vector3(0,0,-sin(mCameraHeading.valueRadians()+3.14/2)*event.timeSinceLastFrame*CAMERA_VEL)*BoostVel;
    }
    if (mKeyA)
    {
        mAuxPosition-=Ogre::Vector3(cos(mCameraHeading.valueRadians())*event.timeSinceLastFrame*CAMERA_VEL,0,0)*BoostVel;
        mAuxPosition-=Ogre::Vector3(0,0,-sin(mCameraHeading.valueRadians())*event.timeSinceLastFrame*CAMERA_VEL)*BoostVel;
    }
    if (mKeyD)
    {
        mAuxPosition+=Ogre::Vector3(cos(mCameraHeading.valueRadians())*event.timeSinceLastFrame*CAMERA_VEL,0,0)*BoostVel;
        mAuxPosition+=Ogre::Vector3(0,0,-sin(mCameraHeading.valueRadians())*event.timeSinceLastFrame*CAMERA_VEL)*BoostVel;
    }
    if(mKeyX)
    {
        mAuxPosition-=Ogre::Vector3(0,CAMERA_VEL*event.timeSinceLastFrame,0)*BoostVel;
    }
    if(mKeySpace)
    {
        mAuxPosition+=Ogre::Vector3(0,CAMERA_VEL*event.timeSinceLastFrame,0)*BoostVel;
    }

    ///Smooth
    mTempAuxPosition+= (mAuxPosition-mTempAuxPosition)*event.timeSinceLastFrame*4;
    ///Smooth
    fTempCameraDistance+= (fCameraDistance-fTempCameraDistance)*event.timeSinceLastFrame*4;

    Ogre::Quaternion ElevationQuat(Ogre::Degree(mRotX), Ogre::Vector3::UNIT_Z);
    Ogre::Quaternion RotationQuat(Ogre::Degree(mRotY), Ogre::Vector3::UNIT_Y);
    Ogre::Quaternion CombinedQuat = RotationQuat*ElevationQuat;

    Ogre::Vector3 start = mTarget->_getDerivedPosition()+mTempAuxPosition;

    mCameraNode->setPosition(start - CombinedQuat*(fTempCameraDistance*Ogre::Vector3::UNIT_X));
    Ogre::Vector3 end = mCameraNode->getPosition();

    /*OgreNewt::BasicRaycast ray(Manager::getWorld(),
                               start,
                               end,
                               true);
    ray.go(Manager::getWorld(),
           start,
           end, true);
    Ogre::Vector3 normalized = (end - start).normalisedCopy();
    if (ray.getHitCount()>0)
    {
        Ogre::Vector3 point = start + (normalized * ((end - start).length() * ray.getInfoAt(1).mDistance));
        mCamera->setPosition(point);
    }
    else*/
    {
        getCamera()->setPosition(mCameraNode->getPosition());
    }

    getCamera()->setOrientation(CombinedQuat);
    getCamera()->lookAt(start);
    mCameraHeading = Ogre::Degree(mCamera->getOrientation().getYaw().valueDegrees());


    /** Atualiza texto da posi�ao **/
    /*GUI::GetWidget<MyGUI::StaticText>("PosX")->setCaption("X: "+Ogre::StringConverter::toString((int)mCamera->getPosition().x));
    GUI::GetWidget<MyGUI::StaticText>("PosY")->setCaption("Y: "+Ogre::StringConverter::toString((int)mCamera->getPosition().y));
    GUI::GetWidget<MyGUI::StaticText>("PosZ")->setCaption("Z: "+Ogre::StringConverter::toString((int)mCamera->getPosition().z));
*/
    return true;
}

void SpaceCamera::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_W)
    {
        mKeyW = true;
    }
    if (event->key() == Qt::Key_S)
    {
        mKeyS = true;
    }
    if (event->key() == Qt::Key_A)
    {
        mKeyA = true;
    }
    if (event->key() == Qt::Key_D)
    {
        mKeyD = true;
    }
    if (event->key() == Qt::Key_Space)
    {
        mKeySpace = true;
    }
    if (event->key() == Qt::Key_X)
    {
        mKeyX = true;
    }
    if(event->key() == Qt::Key_Shift)
    {
        BoostVel = 100;
    }
    //return true;
}

void SpaceCamera::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_W)
    {
        mKeyW = false;
    }
    if (event->key() == Qt::Key_S)
    {
        mKeyS = false;
    }
    if (event->key() == Qt::Key_A)
    {
        mKeyA = false;
    }
    if (event->key() == Qt::Key_D)
    {
        mKeyD = false;
    }
    if (event->key() == Qt::Key_Space)
    {
        mKeySpace = false;
    }
    if (event->key() == Qt::Key_X)
    {
        mKeyX = false;
    }
    if(event->key() == Qt::Key_Shift)
    {
        BoostVel = 1;
    }
    //return true;
}

Ogre::Camera * SpaceCamera::getCamera()
{
    return mCamera;
}




