
/*
 *
 *
 *
 *
 *
 */

#include "InputManager.h"
#include <QForeachContainer>



InputManager *InputManager::mInputManager;

InputManager::InputManager()
: mMouse(0)
, mKeyboard(0)
, mInputSystem(0)
{
}

InputManager::~InputManager()
{
    if(mInputSystem)
    {
        if (mMouse)
        {
            mInputSystem->destroyInputObject(mMouse);
            mMouse = 0;
        }

        if (mKeyboard)
        {
            mInputSystem->destroyInputObject(mKeyboard);
            mKeyboard = 0;
        }

        if (mJoysticks.size() > 0)
        {
            itJoystick    = mJoysticks.begin();
            itJoystickEnd = mJoysticks.end();
            for(; itJoystick != itJoystickEnd; ++itJoystick)
            {
                    mInputSystem->destroyInputObject(*itJoystick);
            }
        }

        mInputSystem->destroyInputSystem(mInputSystem);
        mInputSystem = 0;

        mKeyListeners.clear();
        mMouseListeners.clear();
        mJoystickListeners.clear();
    }
}

InputManager* InputManager::getSingletonPtr()
{
    if (!mInputManager) {
            mInputManager = new InputManager();
    }

    return mInputManager;
}

void InputManager::Init(Ogre::RenderWindow *renderWindow)
{
    if(!mInputSystem)
    {
        OIS::ParamList paramList;
        size_t windowHnd = 0;
        std::ostringstream windowHndStr;
        std::pair<Ogre::String, Ogre::String> p1("w32_mouse", "DISCL_NONEXCLUSIVE");
        std::pair<Ogre::String, Ogre::String> p2("w32_mouse", "DISCL_FOREGROUND");
        paramList.insert(p1);
        paramList.insert(p2);

        renderWindow->getCustomAttribute("WINDOW", &windowHnd);

        windowHndStr << (unsigned int) windowHnd;
        paramList.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

        mInputSystem = OIS::InputManager::createInputSystem(paramList);

        //Ogre::WindowEventUtilities::addWindowEventListener(renderWindow, this);

        //if (mInputSystem->numKeyboards() > 0)
        //{
                mKeyboard = static_cast<OIS::Keyboard*>(mInputSystem->createInputObject(OIS::OISKeyboard, true));
                mKeyboard->setEventCallback(this);
        //}

        //if (mInputSystem->numMice() > 0)
        //{
            mMouse = static_cast<OIS::Mouse*>(mInputSystem->createInputObject(OIS::OISMouse, true));
            mMouse->setEventCallback(this);

            unsigned int width, height, depth;
            int left, top;
            renderWindow->getMetrics(width, height, depth, left, top);

            this->setWindowExtents(width, height);
        //}

        //if (mInputSystem->numJoySticks() > 0)
        //{
                //mJoysticks.resize(mInputSystem->numJoySticks());

            itJoystick    = mJoysticks.begin();
            itJoystickEnd = mJoysticks.end();
            for(; itJoystick != itJoystickEnd; ++itJoystick)
            {
                try
                {
                    (*itJoystick) = static_cast<OIS::JoyStick*>(mInputSystem->createInputObject(OIS::OISJoyStick, true));
                }
                catch (...)
                {
                    (*itJoystick) = 0;
                }

                (*itJoystick)->setEventCallback(this);
            }
        //}
    }
}

void InputManager::Quit()
{
    mQuit = true;
}

void InputManager::Capture()
{
    //if (mMouse) {
        mMouse->capture();
    //}

    //if (mKeyboard) {
        mKeyboard->capture();
    //}

    if (mJoysticks.size() > 0)
    {
        itJoystick    = mJoysticks.begin();
        itJoystickEnd = mJoysticks.end();
        for(; itJoystick != itJoystickEnd; ++itJoystick) {
                (*itJoystick)->capture();
        }
    }
}

void InputManager::addKeyListener(OIS::KeyListener *keyListener, const std::string& name)
{
    if (mKeyboard)
    {
        itKeyListener = mKeyListeners.find(name);
        if (itKeyListener == mKeyListeners.end()) {
                mKeyListeners[name] = keyListener;
        }
        else {
    // Duplicate Item
        }
    }
}

void InputManager::addMouseListener(OIS::MouseListener *mouseListener, const std::string& name)
{
    if (mMouse)
    {
        itMouseListener = mMouseListeners.find(name);
        if (itMouseListener == mMouseListeners.end()) {
                mMouseListeners[name] = mouseListener;
        }
        else {
                // Duplicate Item
        }
    }
}

void InputManager::addJoystickListener(OIS::JoyStickListener *joystickListener, const std::string& name)
{
    if (mJoysticks.size() > 0)
    {
        itJoystickListener = mJoystickListeners.find( name );
        if (itJoystickListener == mJoystickListeners.end()) {
            mJoystickListeners[name] = joystickListener;
        }
        else {
                // Duplicate Item
        }
    }
}
void InputManager::CleanListeners()
{
    foreach (std::string name, mRemoveKeyListeners)
    {
        itKeyListener = mKeyListeners.find(name);
        if (itKeyListener != mKeyListeners.end()) {
                mKeyListeners.erase(itKeyListener);
        }
    }
    foreach (std::string name, mRemoveMouseListeners)
    {
        itMouseListener = mMouseListeners.find( name );
        if (itMouseListener != mMouseListeners.end()) {
                mMouseListeners.erase(itMouseListener);
        }
    }
    foreach (std::string name, mRemoveJoystickListeners)
    {
        itJoystickListener = mJoystickListeners.find(name);
        if (itJoystickListener != mJoystickListeners.end()) {
            mJoystickListeners.erase( itJoystickListener );
        }
    }
    foreach (OIS::KeyListener *keyListener, mRemoveKeyListeners2)
    {
        itKeyListener    = mKeyListeners.begin();
        itKeyListenerEnd = mKeyListeners.end();

        for (; itKeyListener != itKeyListenerEnd; ++itKeyListener)
        {
            if (itKeyListener->second == keyListener)
            {
                    mKeyListeners.erase(itKeyListener);
                    itKeyListener = mKeyListeners.end();
                    break;
            }
        }

    }
    foreach (OIS::MouseListener * mouseListener, mRemoveMouseListeners2)
    {
        itMouseListener    = mMouseListeners.begin();
        itMouseListenerEnd = mMouseListeners.end();

        for(; itMouseListener != itMouseListenerEnd; ++itMouseListener)
        {
                if (itMouseListener->second == mouseListener)
                {
                        mMouseListeners.erase(itMouseListener);
                        break;
                }
        }
    }
    foreach (OIS::JoyStickListener* joystickListener, mRemoveJoystickListeners2)
    {
        itJoystickListener    = mJoystickListeners.begin();
        itJoystickListenerEnd = mJoystickListeners.end();

        for(; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener)
        {
                if (itJoystickListener->second == joystickListener)
                {
                        mJoystickListeners.erase( itJoystickListener);
                        break;
                }
        }
    }

}

void InputManager::removeKeyListener(const std::string& name)
{
    mRemoveKeyListeners.push_back(name);
}

void InputManager::removeMouseListener(const std::string& name)
{
    mRemoveMouseListeners.push_back(name);
}

void InputManager::removeJoystickListener(const std::string& name)
{
    mRemoveJoystickListeners.push_back(name);
}

void InputManager::removeKeyListener(OIS::KeyListener *keyListener)
{
    mRemoveKeyListeners2.push_back(keyListener);
}

void InputManager::removeMouseListener(OIS::MouseListener *mouseListener)
{
    mRemoveMouseListeners2.push_back(mouseListener);
}

void InputManager::removeJoystickListener(OIS::JoyStickListener *joystickListener)
{
    mRemoveJoystickListeners2.push_back(joystickListener);
}

void InputManager::removeAllListeners()
{
    mKeyListeners.clear();
    mMouseListeners.clear();
    mJoystickListeners.clear();
}

void InputManager::removeAllKeyListeners()
{
    mKeyListeners.clear();
}

void InputManager::removeAllMouseListeners()
{
    mMouseListeners.clear();
}

void InputManager::removeAllJoystickListeners()
{
    mJoystickListeners.clear();
}

void InputManager::setWindowExtents(int width, int height)
{
    const OIS::MouseState &mouseState = mMouse->getMouseState();
    mouseState.width  = width;
    mouseState.height = height;
}

OIS::Mouse* InputManager::getMouse()
{
    return mMouse;
}

OIS::Keyboard* InputManager::getKeyboard()
{
    return mKeyboard;
}

OIS::JoyStick* InputManager::getJoystick(unsigned int index)
{
    if (index < mJoysticks.size()) {
            return mJoysticks[index];
    }

    return 0;
}

int InputManager::getNumOfJoysticks()
{
    return (int) mJoysticks.size();
}

bool InputManager::keyPressed(const OIS::KeyEvent &e)
{
    CleanListeners();
    itKeyListener    = mKeyListeners.begin();
    itKeyListenerEnd = mKeyListeners.end();
    std::map<std::string, OIS::KeyListener*>::iterator it;

    for (; itKeyListener != itKeyListenerEnd; ) {
        it = itKeyListener;
            if (!itKeyListener->second->keyPressed(e))
                    break;
                    if(itKeyListener==it)
            ++itKeyListener;
    }

    return true;
}

bool InputManager::keyReleased(const OIS::KeyEvent &e)
{
    CleanListeners();
    itKeyListener    = mKeyListeners.begin();
    itKeyListenerEnd = mKeyListeners.end();

    for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
            if (!itKeyListener->second->keyReleased(e))
                    break;
    }

    return true;
}

bool InputManager::mouseMoved(const OIS::MouseEvent &e)
{
    CleanListeners();
    itMouseListener    = mMouseListeners.begin();
    itMouseListenerEnd = mMouseListeners.end();

    for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
            if (!itMouseListener->second->mouseMoved(e))
                    break;
    }

    return true;
}

bool InputManager::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CleanListeners();
    itMouseListener    = mMouseListeners.begin();
    itMouseListenerEnd = mMouseListeners.end();

    for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
            if (!itMouseListener->second->mousePressed(e, id))
                    break;
    }

    return true;
}
bool InputManager::mouseReleased( const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CleanListeners();
    itMouseListener    = mMouseListeners.begin();
    itMouseListenerEnd = mMouseListeners.end();

    for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
            if (!itMouseListener->second->mouseReleased(e, id))
                    break;
    }

    return true;
}

bool InputManager::povMoved(const OIS::JoyStickEvent &e, int pov)
{
    CleanListeners();
    itJoystickListener    = mJoystickListeners.begin();
    itJoystickListenerEnd = mJoystickListeners.end();

    for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
            if (!itJoystickListener->second->povMoved(e, pov))
                    break;
    }

    return true;
}

bool InputManager::axisMoved(const OIS::JoyStickEvent &e, int axis)
{
    CleanListeners();
    itJoystickListener    = mJoystickListeners.begin();
    itJoystickListenerEnd = mJoystickListeners.end();

    for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
            if (!itJoystickListener->second->axisMoved(e, axis))
                    break;
    }

    return true;
}

bool InputManager::sliderMoved(const OIS::JoyStickEvent &e, int sliderID)
{
    CleanListeners();
    itJoystickListener    = mJoystickListeners.begin();
    itJoystickListenerEnd = mJoystickListeners.end();

    for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
            if (!itJoystickListener->second->sliderMoved(e, sliderID))
                    break;
    }

    return true;
}

bool InputManager::buttonPressed(const OIS::JoyStickEvent &e, int button)
{
    CleanListeners();
    itJoystickListener    = mJoystickListeners.begin();
    itJoystickListenerEnd = mJoystickListeners.end();

    for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
            if (!itJoystickListener->second->buttonPressed(e, button))
                    break;
    }

    return true;
}

bool InputManager::buttonReleased(const OIS::JoyStickEvent &e, int button)
{
    CleanListeners();
    itJoystickListener    = mJoystickListeners.begin();
    itJoystickListenerEnd = mJoystickListeners.end();

    for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
            if (!itJoystickListener->second->buttonReleased(e, button))
                    break;
    }

    return true;
}
