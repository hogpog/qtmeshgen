/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file DelaunayTriangulation.h
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the DelaunayTriangulation class interface.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#ifndef _DELAUNAY_TRIANGULATION_H_
#define _DELAUNAY_TRIANGULATION_H_

#include <vector>

namespace EdgeAlgebra
{

class Site;
class QuadEdge;

/// @brief Triangulates a given array of unordered sites using
/// a delaunay triangulation algorithm. The algorithm used is
/// the one described by Guibas and Stolfi in the paper named
/// "Primitives for the Manipulation of General Subdivisions 
/// and the Computation of Voronoi Diagrams"
class DelaunayTriangulation
{
public:
	/// Constructor
	/// @param sites the array of sites to be triangulated
	DelaunayTriangulation(const std::vector<Site*>& sites);

	/// Destructor
	~DelaunayTriangulation(void);

	/// @brief Gets the left halve of the triangulation
	/// @return left halve of the triangulation
	QuadEdge* GetLeftHalve(void) const;

	/// @brief Gets the right halve of the triangulation
	/// @return right halve of the triangulation
	QuadEdge* GetRightHalve(void) const;

private:
	/// Left halve
	QuadEdge* m_left;

	/// Right halve
	QuadEdge* m_right;

	/// @note hide copy constructor from interface user
	DelaunayTriangulation(const DelaunayTriangulation&);

	/// @note hide assignment operator from interface user
	DelaunayTriangulation& operator=(const DelaunayTriangulation&);

	/// @brief Recursive method to run the divid and conquer
	/// delaunay algorithm.
	/// @param S array of sites
	///	@param sitesLeftIndex left index of the sites
	/// @param halvesSitesCount number of sites in halve
	/// @param le left quadedge
	/// @param re right quadedge
	void RunRecursive(const std::vector<Site*>& S, int sitesLeftIndex, 
						int halveSitesCount, QuadEdge* &le, QuadEdge* &re);

	/// @brief Validate an edge and the related base edge.
	/// It's the same as the one described in Guibas and
	/// Stolfi paper.
	/// @param edge the edge
	/// @param base1 the base edge
	bool Valid(QuadEdge* edge, QuadEdge* base1);
};

}

#endif