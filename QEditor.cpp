#include "QEditor.h"
#include "ui_QEditor.h"
#include "OgreWidget.h"
#include <QVBoxLayout>
#include <QDebug>
#include "QtInputManager.h"
#include <QListView>

QEditor::QEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QEditor)
{
    mOgreWidget = new OgreWidget();
    ui->setupUi(this);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(QMargins(0,0,0,0));
    layout->addWidget(mOgreWidget);
    ui->centralWidget->setLayout(layout);

    //ui->dockWidget->setContentsMargins(QMargins(0,0,0,0));
    //ui->dockWidget->layout()->setContentsMargins(QMargins(0,0,0,0));

    //QListView* meshList = new QListView(ui->MeshDock);
    //QVBoxLayout *layout2 = new QVBoxLayout;
    //layout2->setContentsMargins(QMargins(0,0,0,0));
    //ui->MeshDock->set
    //layout2->addWidget(meshList);
    //ui->MeshDock->setLayout(layout);
}

void QEditor::keyPressEvent(QKeyEvent *event)
{
    QtInputManager::getIntance().keyPressEvent(event);
}

void QEditor::keyReleaseEvent(QKeyEvent *event)
{
    QtInputManager::getIntance().keyReleaseEvent(event);
}

void QEditor::mousePressEvent(QMouseEvent *event)
{
    QtInputManager::getIntance().mousePressEvent(event);
}

void QEditor::mouseReleaseEvent(QMouseEvent *event)
{
    QtInputManager::getIntance().mouseReleaseEvent(event);
}

void QEditor::mouseMoveEvent(QMouseEvent *event)
{
    QtInputManager::getIntance().mouseMoveEvent(event);
}

QEditor::~QEditor()
{
    delete ui;
}
