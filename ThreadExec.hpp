#ifndef _THREADEXEC_HPP_
#define _THREADEXEC_HPP_

#include "ThreadService.h"
#define THREAD_NUM 8

template <class T>
class ThreadExec
{
	public:
		ThreadExec(T func)
		{
		    mFunc = func;
        };
        template <class Y>
        void Execute(Y& deq)
        {
            ThreadServicePtr services[THREAD_NUM];
            for(int x = 0; x< THREAD_NUM;++x)
            {
                services[x] = ThreadService::GetService(std::string("threadexecute_"+x));
            }

            const int size_offset = ((int)deq.size()/THREAD_NUM);
            const int rest = deq.size()-(size_offset*THREAD_NUM);

            for(int x = 0; x < THREAD_NUM; ++x)
            {
                services[x]->Add(boost::bind(&ThreadExec::ExecuteOffset<typename Y::iterator>, this,
                                    (deq.begin()+(x*size_offset)),
                                    (deq.begin()+((x+1)*size_offset+
                                    (THREAD_NUM==(x+1)?rest:0)))));
            }
            for(int x = 0; x< THREAD_NUM;++x)
            {
                services[x]->Join();
            }
        }
	private:
        template <class Tm>
        void ExecuteOffset(Tm begin, Tm end)
        {
            for(;begin!=end;++begin)
            {
                mFunc(*begin);
            }
        }
        T mFunc;
};
#endif
