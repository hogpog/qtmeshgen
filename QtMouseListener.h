#ifndef QTMOUSELISTENER_H
#define QTMOUSELISTENER_H

#include <QMouseEvent>

class QtMouseListener
{
public:
    virtual void mousePressEvent(QMouseEvent *event) = 0;
    virtual void mouseReleaseEvent(QMouseEvent *event) = 0;
    virtual void mouseMoveEvent(QMouseEvent *event) = 0;
};

#endif // QTMOUSELISTENER_H
