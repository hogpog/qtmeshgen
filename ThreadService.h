#ifndef _THREADSERVICE_H_
#define _THREADSERVICE_H_

#include <deque>

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

class ThreadService;
typedef boost::function<void(void)> FunctionCallback;
typedef boost::shared_ptr<ThreadService> ThreadServicePtr;


class ThreadService
{
public:

    void Start();
    void Stop();
    void Join();
    static void DestroyAll();
    static void Destroy(const std::string& serviceName);

    static ThreadServicePtr NewService(const std::string& serviceName);
    static ThreadServicePtr GetService(const std::string& serviceName = "_MainService_");
    static void Add(const FunctionCallback& c, const std::string& serviceName);
    void Add(const FunctionCallback& c);

    ~ThreadService();
private:
    void Service();
    ThreadService();

    static std::map<std::string,ThreadServicePtr> ServiceMap;
    std::deque<FunctionCallback> mCallBackList;

    boost::mutex mMutex;
    bool mWaitQueue;
    bool mContinue;

};

#endif


