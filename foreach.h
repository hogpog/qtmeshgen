#ifndef _FOREACH_H_
#define _FOREACH_H_

#include <deque>
#include <list>
#include <vector>
class Range
{

    public:
    class iterator;
	Range(int value)
	:mValue(value)
	{
	}
	iterator begin() const
	{
		return iterator(0);
	}
	iterator end() const
	{
		return mValue;
	}
	bool empty() const
	{
	    return mValue.mValue <= 0;
	}
	class iterator
	{
	    public:
	    iterator(int value)
	    :mValue(value)
	    {
	    }
	    int mValue;
	    bool operator==(const iterator& a) const
	    {
	        return mValue == a.mValue;
	    }
	    bool operator!=(const iterator& a) const
	    {
	        return mValue != a.mValue;
	    }
	    void operator=(const iterator& a)
	    {
	        mValue = a.mValue;
	    }
	    void operator++()
	    {
	        ++mValue;
	    }
	    int operator*() const
	    {
	       return mValue;
	    }
	};
	iterator mValue;
	typedef const iterator const_iterator;
};
#define foreach(__VAR__, __LIST__) \
typedef typeof(__LIST__) __LISTT__; \
__LISTT__::iterator __it__ = __LIST__.begin(); \
__LISTT__::const_iterator __ite__ = __LIST__.end(); \
if(!__LIST__.empty())\
for(;__VAR__ = *__it__, __it__!=__ite__;++__it__)


#define for_range(__VALUE__, __END__)\
for(;__VALUE__<__END__;++__VALUE__)


#endif
