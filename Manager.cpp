#include "Manager.h"

Ogre::Root* Manager::mRoot=NULL;
Ogre::SceneManager* Manager::mSceneMgr=NULL;
Ogre::RenderWindow* Manager::mWindow=NULL;
InputManager* Manager::mInputManager=NULL;

Ogre::RenderWindow* Manager::getWindow()
{
    return mWindow;
}

void Manager::setWindow(Ogre::RenderWindow *renderWindow)
{
    mWindow = renderWindow;
}

void Manager::setSceneMgr(Ogre::SceneManager *sceneManager)
{
    mSceneMgr = sceneManager;
}

Ogre::SceneManager* Manager::getSceneMgr()
{
    return mSceneMgr;
}

Ogre::Root* Manager::getRoot()
{
    if (!mRoot)
        initRoot();
    return mRoot;
}

void Manager::initRoot()
{
    try
    {
        mRoot = new Ogre::Root("Plugins.cfg", "Video.cfg", "Graphicslog.txt");
        if (!mRoot)
        {
            throw std::logic_error("Erro: Iniciando Root\nFILE: "+std::string(__FILE__)+"\nLINE: "+Ogre::StringConverter::toString(__LINE__));
        }
    }
    catch (std::logic_error const& le)
    {
        #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, (LPCWSTR)le.what(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
        #else
            std::cerr << "An exception has occured: " << le.what().c_str() << std::endl;
        #endif
    }
}


void Manager::initInputManager()
{
    mInputManager = InputManager::getSingletonPtr();

    if(mInputManager)
        mInputManager->Init(getWindow());
}

InputManager* Manager::getInputManager()
{
    if(!mInputManager)
        initInputManager();
    return mInputManager;
}


void Manager::initSceneMgr()
{
    if (!mRoot)
        initRoot();
    try
    {
        mSceneMgr = Manager::getRoot()->createSceneManager("MainSceneManager");

        if (!mSceneMgr)
        {
            throw std::logic_error("Erro: Iniciando SceneManager\nFILE: "+std::string(__FILE__)+"\nLINE: "+Ogre::StringConverter::toString(__LINE__));
        }
    }
    catch (std::logic_error const& le)
    {
        #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, le.what(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
        #else
            std::cerr << "An exception has occured: " << le.what().c_str() << std::endl;
        #endif
    }
}

Manager::~Manager()
{

}
