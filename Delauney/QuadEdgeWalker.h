/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file QuadEdgeWalker.h
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the QuadEdgeWalker class interface.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#ifndef _QUAD_EDGE_WALKER_H_
#define _QUAD_EDGE_WALKER_H_

#include <map>

namespace EdgeAlgebra
{

class QuadEdge;

/// @brief It walks through the triangulation
/// @note Probably its better if it was 
/// named QuadEdgeTraverser
class QuadEdgeWalker
{
public:
	/// Constructor
	/// @param edge starting edge to walk
	QuadEdgeWalker(QuadEdge* edge);
	
	/// Destructor
	~QuadEdgeWalker(void);

	/// @brief Gets the walked edges
	/// @return walked edges
	const std::map<QuadEdge*, char> GetWalkedEdges(void) const;

private:
	/// The walked edges
	std::map<QuadEdge*, char> m_walkedEdges;

	/// @note hide copy constructor from interface user
	QuadEdgeWalker(const QuadEdgeWalker&);

	/// @note hide assignment operator from interface user
	QuadEdgeWalker& operator=(const QuadEdgeWalker&);

	/// @brief Walks the triangulation
	/// @param edge starting edge to walk
	void Walk(QuadEdge* edge);


};

}

#endif