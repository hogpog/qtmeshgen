#ifndef SPACECAMERA_H
#define SPACECAMERA_H

#include "Manager.h"
#include <OgreFrameListener.h>
#include "InputManager.h"
#include <OIS.h>
#include "QtInputManager.h"


class SpaceCamera : public Ogre::FrameListener, public QtMouseListener, public QtKeyListener
{
    public:
        ~SpaceCamera();
        static SpaceCamera* Initialize(Ogre::Camera* camera);
        static SpaceCamera* GetInstance();
        void SetTarget(Ogre::SceneNode* target);
        Ogre::Camera* getCamera();
        ///Ogre::FrameListener
        bool frameStarted(const Ogre::FrameEvent& event);
        bool frameEnded(const Ogre::FrameEvent& event);

        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);

        void mousePressEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);

        ///OIS::MouseListener
        /*bool mouseMoved(const OIS::MouseEvent &e);
        bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool mouseReleased( const OIS::MouseEvent &e, OIS::MouseButtonID id);
        ///OIS::KeyListener
        bool keyPressed(const OIS::KeyEvent &e);
        bool keyReleased(const OIS::KeyEvent &e);*/

    private:
        SpaceCamera(Ogre::Camera *camera);
        static SpaceCamera* mSpaceCamera;
        Ogre::SceneNode *mCameraNode; // Node da camera
        Ogre::Camera *mCamera; // Ogre camera
        Ogre::SceneNode* mTarget;
        Ogre::SceneManager* mSceneMgr; //SceneManager
        ///Controles da camera
        float mRotX, mRotY;
        float fCameraDistance;
        float fTempCameraDistance;
        bool mRightMousePressed;
        Ogre::Radian mHeading;
        Ogre::Radian mCameraHeading;
        Ogre::Vector3 mAuxPosition;
        Ogre::Vector3 mTempAuxPosition;
        int BoostVel;

        int mLastX;
        int mLastY;

        bool mKeyW;
        bool mKeyS;
        bool mKeyA;
        bool mKeyD;
        bool mKeySpace;
        bool mKeyX;
        bool mKeyShift;


};

#endif // SPACECAMERA_H
