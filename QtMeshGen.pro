#-------------------------------------------------
#
# Project created by QtCreator 2011-11-09T00:00:36
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = QtMeshGen
TEMPLATE = app


SOURCES += main.cpp\
        QEditor.cpp \
    OgreWidget.cpp \
    SpaceCamera.cpp \
    Manager.cpp \
    InputManager.cpp \
    QtInputManager.cpp \
    QtKeyListener.cpp \
    QtMouseListener.cpp \
    Delauney/Site.cpp \
    Delauney/QuadEdgeWalker.cpp \
    Delauney/QuadEdge.cpp \
    Delauney/DelaunayTriangulation.cpp \
    ImageDetection.cpp \
    ThreadService.cpp \
    Delaunay.cpp

HEADERS  += QEditor.h \
    OgreWidget.h \
    SpaceCamera.h \
    Manager.h \
    InputManager.h \
    QtInputManager.h \
    QtKeyListener.h \
    QtMouseListener.h \
    Delauney/Site.h \
    Delauney/QuadEdgeWalker.h \
    Delauney/QuadEdge.h \
    Delauney/DelaunayTriangulation.h \
    ImageDetection.h \
    ThreadExec.hpp \
    ThreadService.h \
    foreach.h \
    Delaunay.h


INCLUDEPATH += C:/fernando/boost_1_54_0 \
    C:/fernando/ogre/SDK/include/OGRE \
    C:/fernando/ogre/SDK/include/OIS \
    C:/fernando/ogre/SDK/include

LIBS +=  C:/fernando/ogre/SDK/bin/release/OgreMain.dll \
    C:/fernando/ogre/SDK/bin/release/OIS.dll \
    C:/fernando/boost_1_54_0/bin.v2/libs/system/build/gcc-mingw-4.8.0/release/link-static/threading-multi/libboost_system-mgw48-mt-1_54.a \
    C:/fernando/boost_1_54_0/bin.v2/libs/thread/build/gcc-mingw-4.8.0/release/link-static/threading-multi/libboost_thread-mgw48-mt-1_54.a


FORMS    += QEditor.ui
