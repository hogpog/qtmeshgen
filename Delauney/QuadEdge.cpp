/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification,
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file QuadEdge.cpp
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the QuadEdge class implementation.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#include "QuadEdge.h"

#include <new>
#include <exception>

using namespace EdgeAlgebra;

/// @doc
QuadEdge::QuadEdge(void)  : m_org(0), m_Onext(0),
                            m_orientation(InvSym),
                            m_orientations(new QuadEdge*[4])
{
  m_orientations[0] = this;

  for (int i = 1; i < 4; ++i)
  {
	/// @todo RAII with boost shared_ptr
	/// @todo c++ style cast
	m_orientations[i] = (QuadEdge*)new char[sizeof(QuadEdge)];

    QuadEdge* edge = new (m_orientations[i]) QuadEdge(m_orientations, (Orientation)i);
  }

  m_Onext = this;
  GetRot()->SetOnext(GetInvRot());
  GetSym()->SetOnext(GetSym());
  GetInvRot()->SetOnext(GetRot());
}

QuadEdge::~QuadEdge(void)
{
  if (m_orientation == InvSym)
  {
    for (int i = 1; i < 4; ++i)
    {
      QuadEdge* edge = m_orientations[i];
      edge->~QuadEdge();
      delete[] (char*)edge;
    }

	delete[] m_orientations;
  }
}

Site* QuadEdge::GetOrigin(void) const
{
	return m_org;
}

Site* QuadEdge::GetDestination(void) const
{
	return GetSym()->GetOrigin();
}

QuadEdge* QuadEdge::GetOnext(void) const
{
  return m_Onext;
}

QuadEdge* QuadEdge::GetRnext(void) const
{
	return GetRot()->GetOprev()->GetInvRot();
}

QuadEdge* QuadEdge::GetLnext(void) const
{
  return GetInvRot()->GetOnext()->GetRot();
}

QuadEdge* QuadEdge::GetOprev(void) const
{
  return GetRot()->GetOnext()->GetRot();
}

QuadEdge* QuadEdge::GetRprev(void) const
{
	return GetSym()->GetOnext();
}

QuadEdge* QuadEdge::GetRot(void) const
{
  int index = ((int)m_orientation + EdgeAlgebra::Rot) % 4;
  return m_orientations[index];
}

QuadEdge* QuadEdge::GetSym(void) const
{
  int index = ((int)m_orientation + EdgeAlgebra::Sym) % 4;
  return m_orientations[index];
}

QuadEdge* QuadEdge::GetInvRot(void) const
{
  int index = ((int)m_orientation + EdgeAlgebra::InvRot) % 4;
  return m_orientations[index];
}

void QuadEdge::SetOrigin(Site* origin)
{
  m_org = origin;
}

void QuadEdge::SetDestination(Site* destination)
{
	GetSym()->SetOrigin(destination);
}

void QuadEdge::SetOnext(QuadEdge* Onext)
{
  if (!Onext)
  {
    /// @todo exception managing system....
    throw std::exception();
  }

  m_Onext = Onext;
}

void QuadEdge::Splice(QuadEdge* b)
{
  if (!b)
  {
    /// @todo exception managing system....
    throw std::exception();
  }

  QuadEdge* bOnext = b->GetOnext();
  QuadEdge* aOnext = m_Onext;

  QuadEdge* alpha = m_Onext->GetRot();
  QuadEdge* beta = bOnext->GetRot();

  QuadEdge* alphaOnext = alpha->GetOnext();
  QuadEdge* betaOnext = beta->GetOnext();

  // aOnext = bOnext
  m_Onext = bOnext;

  // bOnext = aOnext
  b->SetOnext(aOnext);

  // alphaOnext = betaOnext
  alpha->SetOnext(betaOnext);

  // betaOnext = alphaOnext
  beta->SetOnext(alphaOnext);
}

QuadEdge* QuadEdge::Connect(QuadEdge* b)
{
  // e <- MakeEdge()
  QuadEdge* e = new QuadEdge();

  // e.Org <- a.Dest
  // e.Dest <- b.Org
  Site* dest = GetDestination();
  e->SetOrigin(dest);
  e->GetSym()->SetOrigin(b->GetOrigin());

  // Splice(e, a.Lnext)
  e->Splice(GetLnext());

  // Splice(e.Sym, b)
  e->GetSym()->Splice(b);

  return e;
}

void QuadEdge::DeleteEdge(void)
{
  // Splice(e, e.Oprev)
  Splice(GetOprev());

  // Splice(e.Sym, e.Sym.Oprev)
  QuadEdge* eSym = GetSym();
  eSym->Splice(eSym->GetOprev());
}

QuadEdge* QuadEdge::GetPtr(void)
{
	return m_orientations[0];
}

QuadEdge::QuadEdge(QuadEdge** orientations, Orientation orientation)  : m_org(0), m_Onext(0),
                                                                        m_orientation(orientation),
                                                                        m_orientations(orientations)
{
}
