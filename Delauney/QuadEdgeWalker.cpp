/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file QuadEdgeWalker.cpp
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the QuadEdgeWalker class implementation.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#include "QuadEdgeWalker.h"
#include "QuadEdge.h"

using namespace EdgeAlgebra;

QuadEdgeWalker::QuadEdgeWalker(QuadEdge* edge)
{
	Walk(edge);
}

QuadEdgeWalker::~QuadEdgeWalker(void)
{
}	

const std::map<QuadEdge*, char> QuadEdgeWalker::GetWalkedEdges(void) const
{
	return m_walkedEdges;
}

void QuadEdgeWalker::Walk(QuadEdge* edge)
{
	/// Check if this edge is already walked
	while (m_walkedEdges.count(edge->GetPtr()) == 0)
	{
		m_walkedEdges[edge->GetPtr()] = '0';

		QuadEdge* nextWalked = edge->GetSym()->GetOnext();
		QuadEdge* oNext = edge->GetOnext();

		Walk(nextWalked);

		edge = oNext;
	}

	return;
}