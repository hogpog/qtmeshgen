
/*
 *
 *
 *
 *
 *
 */

#ifndef _Manager_h
#define _Manager_h


#include <Ogre.h>
#include <OgreException.h>
#include <OgreSingleton.h>
#include <stdexcept>
#include "InputManager.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

class Manager
{
public:

    ~Manager();
    static Ogre::SceneManager* getSceneMgr();
    static void setSceneMgr(Ogre::SceneManager* sceneManager);
    static Ogre::Root* getRoot();
    static Ogre::RenderWindow* getWindow();
    static void setWindow(Ogre::RenderWindow* renderWindow);
    static InputManager* getInputManager();

private:
    static void initRoot();
    static void initSceneMgr();
    static void initInputManager();

    static Ogre::Root* mRoot;
    static Ogre::SceneManager* mSceneMgr;
    static Ogre::RenderWindow *mWindow;
    static InputManager* mInputManager;
};

#endif
