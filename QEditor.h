#ifndef QEDITOR_H
#define QEDITOR_H

#include <QMainWindow>
#include "OgreWidget.h"

namespace Ui {
    class QEditor;
}

class QEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit QEditor(QWidget *parent = 0);
    ~QEditor();

private:
    Ui::QEditor *ui;
    OgreWidget *mOgreWidget;

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
};

#endif // QEDITOR_H
