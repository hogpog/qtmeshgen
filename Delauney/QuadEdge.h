/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file QuadEdge.h
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the QuadEdge class interface.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#ifndef _QUAD_EDGE_H_
#define _QUAD_EDGE_H_

namespace EdgeAlgebra
{

class Site;

enum Orientation
{
  InvSym,
	Rot,
	Sym,
	InvRot
};

class QuadEdge
{
public:
	/// Constructor
	/// It is intended to behave as "e <- MakeEdge()"
	/// as the operator in Guibas and Stolfi paper
	QuadEdge(void);

	/// Destructor
	~QuadEdge(void);

	/// @brief Gets origin of edge
	/// @return origin of edge
	Site* GetOrigin(void) const;

	/// @brief Gets destination of edge
	/// @return destination of edge
	Site* GetDestination(void) const;

	/// @brief Gets onext of edge
	/// @return onext of edge
	QuadEdge* GetOnext(void) const;

	/// @brief Gets rnext of edge
	/// @return rnext of edge
	QuadEdge* GetRnext(void) const;

	/// @brief Gets lnext of edge
	/// @return lnext of edge
	QuadEdge* GetLnext(void) const;

	/// @brief Gets oprev of edge
	/// @return oprev of edge
	QuadEdge* GetOprev(void) const;

	/// @brief Gets rprev of edge
	/// @return rprev of edge
	QuadEdge* GetRprev(void) const;

	/// @brief Gets rotation edge of edge
	/// @return rotation edge of edge
	QuadEdge* GetRot(void) const;

	/// @brief Gets symmetric edge of edge
	/// @return symmetric edge of edge
	QuadEdge* GetSym(void) const;

	/// @brief Gets inverse of rotation edge of edge
	/// @return inverse of rotation edge of edge
	QuadEdge* GetInvRot(void) const;  

	/// @brief Sets the origin
	/// @param origin the origin
	void SetOrigin(Site* origin);

	/// @brief Sets the destination
	/// @param destination the destination
	void SetDestination(Site* destination);

	/// @brief Sets onext edge
	/// @param Onext onext edge
	void SetOnext(QuadEdge* Onext);

	/// @brief Its the same operator as the ones
	/// with the same name described in Guibas and
	/// Stolfi paper
	/// @param h edge
	void Splice(QuadEdge* b);

	/// @brief Its the same operator as the ones
	/// with the same name described in Guibas and
	/// Stolfi paper
	/// @param h edge
	QuadEdge* Connect(QuadEdge* b);

	/// @note This method doesn`t delete the memory
	/// of this edge. It only disconnects the edge
	/// from the other edges.
	void DeleteEdge(void);

	/// The method returns a pointer to the original
	///	quadedge
	QuadEdge* GetPtr(void);

private:
	/// Origin
	Site* m_org;

	/// Onext edge
	QuadEdge* m_Onext;

	/// Orientation of this edge
	Orientation m_orientation;

	/// @todo think about using boost smart pointers => RAII
	QuadEdge** m_orientations;

	/// Used to know who allocated the memory
	bool m_deleteOrientations;

	/// Private constructor
	/// @param orientation the constructed orientations
	/// @param orientation the orientation
	QuadEdge(QuadEdge** orientations, Orientation orientation);

	/// @note hide copy constructor from interface user
	QuadEdge(const QuadEdge&);

	/// @note hide assignment operator from interface user
	QuadEdge& operator=(const QuadEdge&);
};

}

#endif