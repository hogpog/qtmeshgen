#ifndef __OGREWIDGET_H__
#define __OGREWIDGET_H__

#include <OGRE/Ogre.h>
#include <QGLWidget>
#include <QKeyEvent>
#include "QtInputManager.h"
#include <QTimer>
#include <QObject>


#include "Delauney/DelaunayTriangulation.h"
#include "Delauney/QuadEdge.h"
#include "Delauney/QuadEdgeWalker.h"
#include "Delauney/Site.h"
class Triangle
{
public:
    Triangle(EdgeAlgebra::Site* p1, EdgeAlgebra::Site* p2, EdgeAlgebra::Site* p3)
        :mPoint1(p1),mPoint3(p3),mPoint2(p2)
    {

    }
    Triangle(){}
    void SetPoint1(EdgeAlgebra::Site *point)
    {
        mPoint1 = point;
    }

    void SetPoint2(EdgeAlgebra::Site *point)
    {
        mPoint2 = point;
    }

    void SetPoint3(EdgeAlgebra::Site *point)
    {
        mPoint3 = point;
    }

    bool Contains(EdgeAlgebra::Site *point)
    {
        return (point==mPoint1 ||point==mPoint2 ||point==mPoint3);
    }
    bool Equals(Triangle* triangle)
    {
        return (Contains(triangle->mPoint1)&&
                Contains(triangle->mPoint2)&&
                Contains(triangle->mPoint3));
    }

//private:
    EdgeAlgebra::Site *mPoint1, *mPoint2, *mPoint3;
};

class OgreWidget : public QGLWidget, public QtKeyListener, public QtMouseListener
{
  Q_OBJECT

 public:
  OgreWidget( QWidget *parent=0 ):
    QGLWidget( parent ),
    mOgreWindow(NULL),
    mOgreWidgetFocus(true)
    {
        init( "plugins.cfg", "video.cfg", "ogre.log" );
    }

  virtual ~OgreWidget()
    {
      mOgreRoot->shutdown();
      //delete mOgreRoot;
      destroy();
    }
private:
  QTimer *timer;
  bool mOgreWidgetFocus;
  void keyPressEvent(QKeyEvent *event);
  void keyReleaseEvent(QKeyEvent *event);

  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void CreatePoly(std::vector<std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> >& linelist,std::pair<EdgeAlgebra::Site *, EdgeAlgebra::Site *> &baseline);
  void FindCommonPoints(std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> & line);
  std::vector<std::pair<EdgeAlgebra::Site*,EdgeAlgebra::Site*> > mLineList;
  std::vector<Triangle*> mTriangles;

 protected:
  virtual void initializeGL();
  virtual void resizeGL( int, int );
  virtual void paintGL();



  void init( std::string, std::string, std::string );

  virtual Ogre::RenderSystem* chooseRenderer( Ogre::RenderSystemList* );

  Ogre::Root *mOgreRoot;
  Ogre::RenderWindow *mOgreWindow;
  Ogre::Camera *mCamera;
  Ogre::Viewport *mViewport;
  Ogre::SceneManager *mSceneMgr;


};

#endif
