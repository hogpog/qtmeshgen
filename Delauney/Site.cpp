/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file Site.cpp
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the DelaunayTriangulation class implementation.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#include "Site.h"

#include "QuadEdge.h"

using namespace EdgeAlgebra;

Site::Site(void)  : m_x(0.0f), m_y(0.0f)
{
}

Site::Site(float x, float y) : m_x(x), m_y(y)
{
}

Site::Site(float x, float y, float z) : m_x(x), m_y(y), m_z(z)
{
}

Site::Site(float x, float y, float z, QColor cor) : m_x(x), m_y(y), m_z(z), cor(cor)
{
}

Site::Site(const Site& site)  : m_x(site.m_x), m_y(site.m_y)
{
}

Site::~Site(void)
{
}

Site& Site::operator=(const Site& site)
{
  if (this == &site)
  {
    return *this;
  }

  m_x = site.m_x;
  m_y = site.m_y;

  return *this;
}

bool Site::CounterClockWise(Site* b, Site* c)
{
    float x1 = m_x;
    float y1 = m_y;
    float x2 = b->m_x;
    float y2 = b->m_y;
    float x3 = c->m_x; 
    float y3 = c->m_y;

    return (x2 * y3 - y2 * x3) - (x1 * y3 - y1 * x3) + (x1 * y2 - y1 * x2) > 0;
}

bool Site::InCircle(Site* a, Site* b, Site* c)
{
	float x1 = a->m_x;
	float y1 = a->m_y;
    float x2 = b->m_x;
	float y2 = b->m_y;
    float x3 = c->m_x;
	float y3 = c->m_y;
    float x4 = m_x;
	float y4 = m_y;

    return	((y4 - y1) * (x2 - x3) + (x4 - x1) * (y2 - y3)) * ((x4 - x3) * (x2 - x1) - (y4 - y3) * (y2 - y1)) > 
			((y4 - y3) * (x2 - x1) + (x4 - x3) * (y2 - y1)) * ((x4 - x1) * (x2 - x3) - (y4 - y1) * (y2 - y3));
}  

bool Site::RightOf(const QuadEdge& edge)
{
	return CounterClockWise(edge.GetDestination(), edge.GetOrigin());
}

bool Site::LeftOf(const QuadEdge& edge)
{
	return CounterClockWise(edge.GetOrigin(), edge.GetDestination());
}

bool EdgeAlgebra::SiteLessThan(EdgeAlgebra::Site* left, EdgeAlgebra::Site* right)
{
	/// @todo comparing two floats... thats not very nice....!
	if (left->m_x == right->m_x)
	{
		return left->m_y < right->m_y;
	}

	return left->m_x < right->m_x;
}
