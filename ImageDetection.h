#ifndef IMAGEDETECTION_H
#define IMAGEDETECTION_H
#include "deque"
#include "map"
#include <math.h>
#include <QPoint>
#include <QColor>
#include <QImage>
#include <QMutex>

class pontos
{
public:
    QPoint a,b;
    QColor c;
    pontos(QPoint A, QPoint B, QColor C){a=A;b=B;c=C;}
};

class ImageDetection
{
    QImage img;
    QImage img2;
    QImage img3;
    QMutex mMutex;
    int quadrado;
public:
    ImageDetection();
    void Detect(const QPoint& lista);
    std::deque<pontos> lista3;
    std::deque<pontos> getPoints(){return lista3;}
};

#endif // IMAGEDETECTION_H
