/// Copyright (c) 2009 Ruben Penalva. All rights reserved.
/// Redistribution and use in source and binary forms, with or without modification, 
/// are permitted provided that the following conditions are met:
///    1. Redistributions of source code must retain the above copyright notice, this 
///    list of conditions and the following disclaimer.
///    2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation and/or
///    other materials provided with the distribution.
///    3. The name of the author may not be used to endorse or promote products derived
///    from this software without specific prior written permission.
///
/// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
/// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
/// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
/// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
/// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
/// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
/// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file DelaunayTriangulation.cpp
/// This source file is part of 2dRender Delaunay Test.
/// The file holds the DelaunayTriangulation class implementation.
/// @author Ruben Penalva (ruben.penalva@rpenalva.com)
#include "DelaunayTriangulation.h"

#include "QuadEdgeWalker.h"
#include "Site.h"
#include "QuadEdge.h"

using namespace EdgeAlgebra;

DelaunayTriangulation::DelaunayTriangulation(const std::vector<Site*>& sites) : m_left(0), m_right(0)
{
	RunRecursive(sites, 0, sites.size(), m_left, m_right);
}

DelaunayTriangulation::~DelaunayTriangulation(void)
{
	QuadEdgeWalker walker(m_left);	
	
	const std::map<QuadEdge*, char>& edges = walker.GetWalkedEdges();

	std::map<QuadEdge*, char>::const_iterator it = edges.begin();
	std::map<QuadEdge*, char>::const_iterator itEnd = edges.end();
	while (it != itEnd)
	{
		delete it->first;
		++it;
	}
}

QuadEdge* DelaunayTriangulation::GetLeftHalve(void) const
{
	return m_left;
}

QuadEdge* DelaunayTriangulation::GetRightHalve(void) const
{
	return m_right;
}

void DelaunayTriangulation::RunRecursive(const std::vector<Site*>& S, 
										 int sitesLeftIndex, int halveSitesCount,
										 QuadEdge* &le, QuadEdge* &re)
{
	/// Recursive stop condition
	if (halveSitesCount == sitesLeftIndex + 2)
	{
		/// Let s1, s2 be the two sites, in sorted order. 
		Site* s1 = S[sitesLeftIndex];
		Site* s2 = S[sitesLeftIndex + 1];
		/// Create an edge a from a1 to s2
		QuadEdge* a = new QuadEdge();
		a->SetOrigin(s1);
		a->SetDestination(s2);
		
		le = a;
		re = a->GetSym();

		return;
	}
	/// Recursive stop condition
	else if (halveSitesCount == sitesLeftIndex + 3)
	{
		/// Let s1, s2, s3 be the three sites, in sorted order		
		Site* s1 = S[sitesLeftIndex];
		Site* s2 = S[sitesLeftIndex + 1];
		Site* s3 = S[sitesLeftIndex + 2];

		/// Create edges a connecting s1 to s2 and b connecating s2 to s3
		QuadEdge* a = new QuadEdge();
		QuadEdge* b = new QuadEdge();
		a->GetSym()->Splice(b);
		a->SetOrigin(s1);
		a->SetDestination(s2);
		b->SetOrigin(s2);
		b->SetDestination(s3);

		/// Now close the triangle
		if (s1->CounterClockWise(s2, s3))
		{
			QuadEdge* c = b->Connect(a);
			
			le = a;
			re = b->GetSym();
		}
		else if (s1->CounterClockWise(s3, s2))
		{
			QuadEdge* c = b->Connect(a);

			le = c->GetSym();
			re = c;
		}
		/// The three points are collinear
		else
		{
			le = a;
			re = b->GetSym();
		}
	
		return;
	}
	/// Recursion
	else
	{
		/// |S| >= 4. Let L and R be the left and right halves of S
		QuadEdge* ldo = 0; 
		QuadEdge* ldi = 0;
		QuadEdge* rdo = 0;
		QuadEdge* rdi = 0;
		
		int newHalveSitesCount = (sitesLeftIndex + halveSitesCount) / 2;
		RunRecursive(S, sitesLeftIndex, newHalveSitesCount, ldo, ldi);
		RunRecursive(S, newHalveSitesCount, halveSitesCount, rdi, rdo);

		/// Compute the lower common tangent of L and R
		while (1)
		{
			if (rdi->GetOrigin()->LeftOf(*ldi))
			{
				ldi = ldi->GetLnext();
			}
			else if (ldi->GetOrigin()->RightOf(*rdi))
			{
				rdi = rdi->GetRprev();
			}
			else
			{
				break;
			}
		}

		/// Create a first cross edge base1 from rdi.Org to ldi.Org
		QuadEdge* base1 = rdi->GetSym()->Connect(ldi);
		if (ldi->GetOrigin() == ldo->GetOrigin())
		{
			ldo = base1->GetSym();
		}
		if (rdi->GetOrigin() == rdo->GetOrigin())
		{
			rdo = base1;
		}

		/// This is the merge loop
		while (1)
		{
			/// Locate the first L point (lcand.Dest) to  be encountered by
			/// the rising bubble and delete L edges out of base1.Dest that
			/// fail the circle test
			QuadEdge* lcand = base1->GetSym()->GetOnext();
			if (Valid(lcand, base1))
			{
				while (lcand->GetOnext()->GetDestination()->InCircle(base1->GetDestination(), 
																	 base1->GetOrigin(), 
																	 lcand->GetDestination()))
				{
					QuadEdge* t = lcand->GetOnext();
					lcand->DeleteEdge();
					delete lcand->GetPtr();
					 
					lcand = t;
				}
			}

			/// Symmetrically, locate the first R point to be hit, and delete R edges
			QuadEdge* rcand = base1->GetOprev();
			if (Valid(rcand, base1))
			{
				while (rcand->GetOprev()->GetDestination()->InCircle(base1->GetDestination(), 
																	 base1->GetOrigin(), 
																	 rcand->GetDestination()))
				{
					QuadEdge* t = rcand->GetOprev();
					rcand->DeleteEdge();
					delete rcand->GetPtr();

					rcand = t;
				}
			}

			/// If both lcand and rcand are invalid, then base1 is the upper common tangent
			if (!Valid(lcand, base1) && !Valid(rcand, base1))
			{
				break;
			}

			/// The next cross edge is to be connected to either lcand.Dest or rcand.Dest
			/// If both are valid, then choose the appropriate one using the InCircle test
			if (!Valid(lcand, base1) || 
				(Valid(rcand, base1) && rcand->GetDestination()->InCircle(lcand->GetDestination(),
																		  lcand->GetOrigin(), 
																		  rcand->GetOrigin())))
			{
				/// Add cross edge base1 from rcand.Dest to base1.Dest
				base1 = rcand->Connect(base1->GetSym());
			}
			else
			{
				/// Add cross edge base1 from base1.Org to lcand.Dest
				base1 = base1->GetSym()->Connect(lcand->GetSym());
			}
		}

		le = ldo;
		re = rdo;
	}
}

bool DelaunayTriangulation::Valid(QuadEdge* edge, QuadEdge* base1)
{
	return edge->GetDestination()->RightOf(*base1);
}